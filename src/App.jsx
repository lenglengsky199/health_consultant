import { Route, Routes } from "react-router-dom";

import Home from "./component/Home";
import Navigation from "./Header/Navigation";
import LoginScrenn from "./pages/LoginScreen";
import Team from "./component/Team";
import Doctor from "./pages/Doctor";
import Map from "./pages/Map";
import Axiety from "./solutions/Axiety";
import PTSD from "./solutions/PTSD";
import ADHD from "./solutions/ADHD";
import Testing from "./component/Testing";
import OCD from "./solutions/OCD";
import Depress from "./solutions/Depress";



function App() {
  return (
    <>
      <Navigation />

      
      <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/testing" element={<Testing/>} />
    
       <Route path="/login" element={<LoginScrenn />}/> 
       <Route path="/doctor" element={<Doctor />}/> 
       <Route path="/team" element={ <Team />}/> 
       <Route path="/map" element={ <Map/>}/>  
       <Route path="/axiety" element={ <Axiety/>}/>
       <Route path="/ptsd" element={ <PTSD/>}/>
       <Route path="/adhd" element={ <ADHD/>}/>
       <Route path="/ocd" element={ <OCD/>}/>
       <Route path="/depression" element={ <Depress/>}/>
      
      
   
      </Routes>
    
    </>
  );
}

export default App;
