import OuchSolita from "../assets/Ouch Solita.jpg";
import SrunSreyphat from "../assets/Srun Sreyphat.jpg";
import SophaonSreyneath from "../assets/Sophaon Sreyneath.jpg";
import VannakNobelShili from "../assets/Vannak Nobel Shili.jpg";
import YounSinHong from "../assets/Youn SinHong.jpg";
import NethPhannaVong from "../assets/Neth PhannaVong.jpg";
import VenSovannaroth from "../assets/Ven Sovannaroth.jpg";
import SongSeytavan from "../assets/Song Seytavan.jpg";
import ChhorLycheng from "../assets/Chhor Lycheng.jpg";
import VanMorokot from "../assets/Van Morokot.jpg";
import PovBunthoeun from '../assets/Dr. Pov Bunthoeun.jpg'
import KertChhutly from '../assets/Dr.Kert Chhutly.jpg'
export default function Team() {
  return (
    <>
      <section class="bg-white dark:bg-gray-900">
        <div class="container px-6 py-8 mx-auto">
          <h2 class="text-3xl font-semibold text-center text-gray-800 capitalize lg:text-4xl dark:text-white">
          Cooperate docotor
          </h2>

          <div class="grid gap-8 mt-8 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
          <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={PovBunthoeun}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  OuchSolita
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  25 Years old Graduated from Psychology Department at Royal
                  University of Phnom Penh.
                </span>
                <h4></h4>
              </div>
            </div>

            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={OuchSolita}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  OuchSolita
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  25 Years old Graduated from Psychology Department at Royal
                  University of Phnom Penh.
                </span>
                <h4></h4>
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={KertChhutly}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  OuchSolita
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  25 Years old Graduated from Psychology Department at Royal
                  University of Phnom Penh.
                </span>
                <h4></h4>
              </div>
            </div>


            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={SrunSreyphat}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Srun Sreyphat
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  24 Years old Graduated from Department of Psychology at
                  Thammasat University, Thailand.
                </span>
              </div>
            </div>

            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={SophaonSreyneath}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Sophaon Sreyneath
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  {" "}
                  23 Years Old Senior Student of Psychology Department at Royal
                  University of Phnom Penh
                </span>
              </div>
            </div>

            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={VannakNobelShili}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Vannak NobelShili
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  {" "}
                  23 Years Old Senior Student from Department of Psychology at
                  University of Melbourne, Australia{" "}
                </span>
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={YounSinHong}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  oun SinHong
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  26 Years Old Graduated from Psychology Department at The
                  University of Queensland, Australia
                </span>
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={NethPhannaVong}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Neth PhannaVong
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  {" "}
                  25 Years Old Graduated from Bachelor Program in Psychology at
                  Silpakorn University
                </span>
              
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={VenSovannaroth}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Ven Sovannaroth
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  22 Years Old Fresh graduate student from Department of
                  Psychology at Thammasat University
                </span>
                <h4>Range: 10 dollars per hour</h4>
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={SongSeytavan}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Song Seytavan
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  , 25 Years Old Graduated Student from Psychology Department at
                  Royal University of Phnom Penh.
                </span>
                <h4>Range: 10 dollars per hour</h4>
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={ChhorLycheng}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Chhor Lycheng
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  23 Years old Senior Student from Psychology Department at
                  Royal University of Phnom Penh
                </span>
                <h4>Range: 10 dollars per hour</h4>
              </div>
            </div>
            <div class="w-full max-w-xs text-center">
              <img
                class="object-cover object-center w-full h-48 mx-auto rounded-lg"
                src={VanMorokot}
                alt="avatar"
              />

              <div class="mt-2">
                <h3 class="text-lg font-medium text-gray-700 dark:text-gray-200">
                  Van Morokot
                </h3>
                <span class="mt-1 font-medium text-gray-600 dark:text-gray-300">
                  22 Years Old Graduated from College of Science at Tsinghua
                  University, China
                </span>
                <h4>Range: 10 dollars per hour</h4>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
