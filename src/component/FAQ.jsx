import React from "react";

export default function FAQ() {
  return (
    <div>
      <section class="bg-white dark:bg-gray-900">
        <div class="container px-6 py-12 mx-auto">
          <h1 class="text-3xl font-semibold text-gray-800 lg:text-4xl dark:text-white">
            Frequently asked questions
          </h1>
          <div class="mt-8 space-y-8 lg:mt-12">
            <div class="p-8 bg-gray-100 rounded-lg dark:bg-gray-800">
              <button class="flex items-center justify-between w-full">
                <h1 class="font-semibold text-gray-700 dark:text-white">
                  What does ប្រឹក្សា-consult offer
                </h1>

                <span class="text-gray-400 bg-gray-200 rounded-full">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M18 12H6"
                    />
                  </svg>
                </span>
              </button>

              <p class="mt-6 text-sm text-gray-500 dark:text-gray-300">
                We offer a phone based consulting service, and the fee is
                charged hourly.
              </p>
            </div>
            <div class="p-8 bg-gray-100 rounded-lg dark:bg-gray-800">
              <button class="flex items-center justify-between w-full">
                <h1 class="font-semibold text-gray-700 dark:text-white">
                  I have created my account, what should I do next?
                </h1>

                <span class="text-gray-400 bg-gray-200 rounded-full">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M18 12H6"
                    />
                  </svg>
                </span>
              </button>

              <p class="mt-6 text-sm text-gray-500 dark:text-gray-300">
                After registering, you may begin your consulting session right
                away by selecting one of the experts provided, creating an
                appointment, and waiting for it to be confirmed.
              </p>
            </div>
            <div class="p-8 bg-gray-100 rounded-lg dark:bg-gray-800">
              <button class="flex items-center justify-between w-full">
                <h1 class="font-semibold text-gray-700 dark:text-white">
                  What is the difference between standard and premium plan?
                </h1>

                <span class="text-gray-400 bg-gray-200 rounded-full">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M18 12H6"
                    />
                  </svg>
                </span>
              </button>

              <p class="mt-6 text-sm text-gray-500 dark:text-gray-300">
                With our standard plan, you get limited access to our website.
                To upgrade, get our premium plan to get full access to all of
                our consultants in our website.
              </p>
            </div>
            <div class="p-8 bg-gray-100 rounded-lg dark:bg-gray-800">
              <button class="flex items-center justify-between w-full">
                <h1 class="font-semibold text-gray-700 dark:text-white">
                  {" "}
                  How do I make payment?
                </h1>

                <span class="text-gray-400 bg-gray-200 rounded-full">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M18 12H6"
                    />
                  </svg>
                </span>
              </button>

              <p class="mt-6 text-sm text-gray-500 dark:text-gray-300">
                After choosing the consultant, you then can proceed to make an
                appointment and pay directly to our consultant with the payment
                methods they provide.
              </p>
            </div>
           
            <div class="p-8 bg-gray-100 rounded-lg dark:bg-gray-800">
              <button class="flex items-center justify-between w-full">
                <h1 class="font-semibold text-gray-700 dark:text-white">
                  {" "}
                  How can I cancel my subscription?
                </h1>

                <span class="text-gray-400 bg-gray-200 rounded-full">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    class="w-6 h-6"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      d="M18 12H6"
                    />
                  </svg>
                </span>
              </button>

              <p class="mt-6 text-sm text-gray-500 dark:text-gray-300">
                When your subscription ends, it is automatically cancelled, and
                patients must renew the subscription monthly to continue using
                it.
              </p>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}
