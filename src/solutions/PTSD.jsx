import React from "react";
import SrunSreyphat from "../assets/Srun Sreyphat.jpg";
export default function PTSD() {
  return (
    <div>
      <>
        <section class="bg-white dark:bg-gray-900">
          <div class="container px-6 py-10 mx-auto">
            <h1 class="text-3xl font-semibold text-gray-800 capitalize lg:text-4xl dark:text-white">
              Experience Adviser
            </h1>

            <div class="mt-8 lg:-mx-6 lg:flex lg:items-center">
              <img
                class="object-cover w-full lg:mx-6 lg:w-1/2 rounded-xl h-72 lg:h-96"
                src={SrunSreyphat}
              />

              <div class="mt-6 lg:w-1/2 lg:mt-0 lg:mx-6 ">
                <p class="text-sm text-blue-500 uppercase">category</p>

                <a
                  href="#"
                  class="block mt-4 text-2xl font-semibold text-gray-800 hover:underline dark:text-white md:text-3xl"
                >
                  All about PTSD
                </a>

                <p class="mt-3 text-sm text-gray-500 dark:text-gray-300 md:text-sm">
                  Home / Health Conditions / Mental Health / These PTSD Signs
                  Are Commonly Overlooked In Children These PTSD Signs Are
                  Commonly Overlooked In Children “This could be because
                  parental stress early on is worsened by their children’s
                  symptoms, or because the child’s responses are shaped by their
                  parents’ initial reactions — or a bit of both, leading to an
                  amplification of symptoms for both parties,” he said.
                  “Interestingly, even in these cases, the parents were still
                  unlikely to acknowledge their children’s suffering,”
                  Meiser-Stedman added. “This study strengthens the case for
                  considering parental mental health, and providing support for
                  both children and their parents in the aftermath of a trauma
                  to reduce the long-term effects for both,” he concluded.
                </p>

                <a
                  href="#"
                  class="inline-block mt-2 text-blue-500 underline hover:text-blue-400"
                >
                  Read more
                </a>

                <div class="flex items-center mt-6">
                  {/* <img class="object-cover object-center w-10 h-10 rounded-full" src="https://images.unsplash.com/photo-1531590878845-12627191e687?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80"/> */}

                  <div class="mx-4">
                    <h1 class="text-sm text-gray-700 dark:text-gray-200">
                    Srun Sreyphat
                    </h1>
                    <p class="text-sm text-gray-500 dark:text-gray-400">
                      PTSD Consultant
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <button className="bg-orange-500 text-center">
              start Appointment
            </button>
          </div>
        </section>
      </>
    </div>
  );
}
