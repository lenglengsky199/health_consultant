import React from "react";
import OuchSolita from "../assets/Ouch Solita.jpg";
export default function ADHD() {
  return (
    <>
      <section class="bg-white dark:bg-gray-900">
        <div class="container px-6 py-10 mx-auto">
          <h1 class="text-3xl font-semibold text-gray-800 capitalize lg:text-4xl dark:text-white">
            Experience Doctor
          </h1>

          <div class="mt-8 lg:-mx-6 lg:flex lg:items-center">
            <img
              class="object-cover w-full lg:mx-6 lg:w-1/2 rounded-xl h-72 lg:h-96"
              src={OuchSolita }
            />

            <div class="mt-6 lg:w-1/2 lg:mt-0 lg:mx-6 ">
              <p class="text-sm text-blue-500 uppercase">category</p>

              <a
                href="#"
                class="block mt-4 text-2xl font-semibold text-gray-800 hover:underline dark:text-white md:text-3xl"
              >
                All about ADHD
              </a>

              <p class="mt-3 text-sm text-gray-500 dark:text-gray-300 md:text-sm">
                Because the condition can be difficult to diagnose, particularly
                in adults, it’s also crucial that you work with a professional
                whose trained to rule out other conditions and make an
                appropriate diagnosis. A good place to start? Attention Deficit
                Disorder Association or your local Mental Health Association can
                refer you to a general physician, psychologist, or psychiatrist
                equipped with the knowledge to reach a proper diagnosis.
                Meanwhile, the cost of going untested or untreated could result
                in: Addiction to alcohol, tobacco, and drugs in an effort to
                self-medicate. Never reaching your full potential in your
                career, relationships or education. Financial woes due to
                impulsivity. Repeated patterns of relationship issues, including
                divorce. Health problems due to compulsive poor eating,
                neglecting exercise, or forgetting to take important medication.
                Don’t get caught up on the negative stigma associated with ADHD.
                Get tested today!
              </p>

              <a
                href="#"
                class="inline-block mt-2 text-blue-500 underline hover:text-blue-400"
              >
                Read more
              </a>

              <div class="flex items-center mt-6">
                {/* <img class="object-cover object-center w-10 h-10 rounded-full" src="https://images.unsplash.com/photo-1531590878845-12627191e687?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80"/> */}

                <div class="mx-4">
                  <h1 class="text-sm text-gray-700 dark:text-gray-200">
                    Jame Sbon
                  </h1>
                  <p class="text-sm text-gray-500 dark:text-gray-400">
                    ADHD Consultant
                  </p>
                </div>
              </div>
            </div>
          </div>

          <button className="bg-orange-500 text-center">
            start consultant
          </button>
        </div>
      </section>
    </>
  );
}
